export function genRoot(items: Record<string, string>, message = '') {
  return `
  <!DOCTYPE html>
  <html>
      <head>
        <meta charset="UTF-8">
          <title>
            List
          </title>
      </head>
      <body>
        <form action="/" method="post">
          <div class="form-example">
            <label for="key">ID: </label>
            <input type="text" name="key" id="key" required maxlength="10" pattern="^[0-9A-Za-z]+$">
          </div>
          <div class="form-example">
            <label for="value">表示: </label>
            <input type="text" name="value" id="value" required maxlength="10">
          </div>
          <div class="form-example">
            <input type="submit" value="登録">
          </div>
        </form>
        ${message ? `<p style="color:red;">${message}</p>` : ''}
        <ul>
          ${Object.entries(items)
            .map(([key, value]) => `<li><a href="${key}">${value}</a></li>`)
            .join('\n')}
          </ul>
      </body>
  </html>`
}
