export function genOne(item: { key: string; value: string }) {
  return `
  <!DOCTYPE html>
  <html>
      <head>
          <meta charset="UTF-8">
          <title>
          ${item.value}
          </title>
      </head>
      <body>
        <h1>${item.value}</h1>
        <h2>${item.key}</h2>
        <a href="/">もどる</a>
      </body>
  </html>`
}
