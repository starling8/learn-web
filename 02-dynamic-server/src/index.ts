import * as Express from 'express'
import * as bodyParser from 'body-parser'
import { genOne, genRoot, notFound } from './templates'

const app = Express()
const port = 3000

const items: Record<string, string> = {
  '1': '１ばんめ',
  '2': '２ばんめ',
  '3': '３ばんめ',
}

app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (_, res) => {
  const template = genRoot(items)
  res.send(template)
})

app.post('/', (req, res) => {
  let valid = true
  const key: string = req.body?.key?.toString() || ''
  valid = /^[a-zA-Z0-9]+$/.test(key)
  valid = valid && key.length <= 10
  valid = valid && !items[key]
  const value: string = req.body?.key?.toString() || ''
  valid = valid && value.length > 0 && value.length <= 10
  if (valid) {
    items[key] = value
    res.redirect(`/${key}`)
  } else {
    const exists = items[key]
    const message = `にゅうりょくにあやまりがあります${
      exists ? `、${key}はすでに登録されています` : ''
    }`
    const template = genRoot(items, message)
    res.send(template)
  }
})

app.get('/:id', (req, res) => {
  const value = items[req.params.id]
  if (value) {
    const template = genOne({ key: req.params.id, value })
    res.send(template)
  } else {
    res.status(404).send(notFound)
  }
})

app.get('*', (_, res) => {
  res.send(notFound)
})

app.listen(port)
