let toggle = false;

document.addEventListener("DOMContentLoaded", function () {
  const button = document.getElementById("button");
  if (button) {
    button.addEventListener("click", function () {
      const title = document.getElementById("title");
      if (title) {
        title.style.color = toggle ? "red" : "blue";
        toggle = !toggle;
      }
    });
  }
});
